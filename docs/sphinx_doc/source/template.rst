template package
================

Submodules
----------

template.example\_class module
------------------------------

.. automodule:: template.example_class
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: template
   :members:
   :undoc-members:
   :show-inheritance:
