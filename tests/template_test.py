from src.template import __main__


def test_main():
    """Test the main function."""
    result = __main__.main()
    expected_result = "This is a template project."
    assert result == expected_result
