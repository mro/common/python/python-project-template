# Python Project Template

This is a project template for Python-based projects.

## Getting started

- Fork this project or download it;
- Edit this README by following the template [below](https://gitlab.cern.ch/mro/common/python/python-project-template#project-name);
- Find the `EDITME` label in the project, replace the text in paretheses with your own text, and remove the label.
- If you are a pro of the Pyhton template and don't need `EDITME` comments to remind you what to do, erase them all with the command:

```bash
bash ./scritps/strip_editme.sh
```

# Project name

`EDITME`( Choose a self-explaining name for your project, then add here a brief description. Provide context and add a link to relevant references that visitors might be unfamiliar with. )

## Requirements

`EDITME`( Add here the list of requirements for your project. If your project involves also hardware requirements, split them into two categories, Software and Hardware, as the following example shows. The required Python libraries and their versions should not be added here, but in the `requirements.txt` file. )

**Hardware**

* None

**Software**

* Python(3.8 or higher)

## Getting started

`EDITME`( Explain here how to get started with your project by listing a number of actions that should trivially allow anyone to run your project. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. Follow the example below. )

* Clone this repository in local, then get into it with the following commands:

```
git clone ssh://git@gitlab.cern.ch:7999/mro/python-project-template.git
cd python-project-template
```

* Install `poetry` if you haven't already:

```
pip install poetry
```

* Install the `poetry-plugin-export` plugin:

```
poetry self add poetry-plugin-export
```

* Install the dependencies and create a virtual environment, then activate the virtual environment with:

```
poetry install
poetry shell
```

* Run the project with:
```
python src/template/__main__.py
```

## Contributing

* If you wish to contribute, install development requirements with:
```
poetry install --with dev
```

* Create a branch with a short and meaningful name

* Develop your feature

* Lint, style, and test the project with:
```
poetry run python setup.py lint
poetry run python setup.py style
poetry run python setup.py test
```

* Generate the documentation with:
```
poetry run python setup.py doc
```

* Export the requirements files:
```
./export_requirements.sh
```

## Usage

`EDITME`( Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README. )

## Roadmap

`EDITME`( If the project is under development and subject to ongoing changes, it is a good idea to let the users know it. You can clarify here what features are available and add a list of features that you intend to develop. )

## Authors and contacts

`EDITME`( List here the main contact people for this project and their email. )

## License

`EDITME`( Add the name of the license here if it is a standard one, or add a custom `LICENSE` file instead. )
