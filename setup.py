#!/usr/bin/env python

"""The setup script."""

import sys
import os
import shutil
from setuptools import setup, find_packages, distutils
import subprocess


SRCDIR = "src"
SOURCE_SPHINX = "./docs/sphinx_doc/source"
BUILD_SPHINX = "./docs/sphinx_doc/build"

KEYWORDS = [
    "python3",
    "project-template",
]

PLATFORMS = ["Windows", "Linux", "Mac OS-X"]

with open("requirements.txt", encoding="utf-8") as requirements_file:
    requirements = requirements_file.read()

with open("requirements-dev.txt", encoding="utf-8") as requirements_file:
    test_requirements = requirements_file.read()


class TestScript(distutils.cmd.Command):
    """Run tests using pytest"""

    user_options: list = []

    def initialize_options(self):
        pass

    def finalize_options(self):
        pass

    def run(self):
        errno = subprocess.call(["pytest", "tests"])
        raise SystemExit(errno)


class StyleScript(distutils.cmd.Command):
    """Run code-style program"""

    user_options: list[str] = []

    def initialize_options(self):
        pass

    def finalize_options(self):
        pass

    def run(self):
        distutils.spawn.spawn(["black", "."])


class LintScript(distutils.cmd.Command):
    """Run linting program"""

    user_options: list[str] = []

    def initialize_options(self):
        pass

    def finalize_options(self):
        pass

    def run(self):
        distutils.spawn.spawn(["ruff", "check", SRCDIR])


class DocScript(distutils.cmd.Command):
    """Build Sphinx documentation"""

    user_options: list[str] = []

    def initialize_options(self):
        pass

    def finalize_options(self):
        pass

    def run(self):
        # Remove the existing build directory
        build_dir = os.path.join(os.path.dirname(__file__), 'docs/sphinx_doc', 'build')
        if os.path.exists(build_dir):
            shutil.rmtree(build_dir)

        # Generate the reStructuredText files
        source_dir = os.path.join(os.path.dirname(__file__), 'docs/sphinx_doc', 'source')
        apidoc_cmd = [
            'sphinx-apidoc',
            '-o', source_dir,
            os.path.join(os.path.dirname(__file__), 'src/template')
        ]
        subprocess.check_call(apidoc_cmd)

        # Run the Sphinx build
        errno = subprocess.call(["sphinx-build", "-b", "html", source_dir, build_dir])
        raise SystemExit(errno)

setup(
    name="template",
    keywords=KEYWORDS,
    description="Template for Python projects",
    url="https://gitlab.cern.ch/mro/common/python/python-project-template",
    version="1.0.0",
    author="Enrica Soria",
    author_email="enrica.soria@cern.ch",
    python_requires=">=3.10",
    install_requires=requirements,
    platforms=PLATFORMS,
    packages=find_packages(where="src"),
    package_dir={"": "src"},
    include_package_data=True,
    zip_safe=False,
    cmdclass={
        "style": StyleScript,
        "lint": LintScript,
        "doc": DocScript,
        "test": TestScript,
    },
)
