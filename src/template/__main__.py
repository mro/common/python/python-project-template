"""Main module of the project."""


def main():
    """Main function of the project"""

    string = "This is a template project."
    print(string)
    return string


if __name__ == "__main__":
    main()
