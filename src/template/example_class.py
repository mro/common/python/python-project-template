class ExampleClass:
    """
    This is an example class.

    Attributes:
        attribute1 (str): Description of attribute1.
        attribute2 (int): Description of attribute2.
    """

    def __init__(self, attribute1, attribute2):
        """
        The constructor for ExampleClass.

        Parameters:
            attribute1 (str): Description of attribute1.
            attribute2 (int): Description of attribute2.
        """
        self.attribute1 = attribute1
        self.attribute2 = attribute2

    def example_method(self):
        """
        An example method.

        Returns:
            str: Description of return value.
        """
        return f"Attribute1 is {self.attribute1} and Attribute2 is {self.attribute2}"
    