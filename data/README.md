# Data

`EDITME`( You can delete this file or update it with an explanation of this folder's content. Please, place in this folder data files such as txt, csv, hdf, that are used or generated by your project. Be parsimonius with data files that you include to this repository to keep the project to a reasonable size. Remeber that data that you don't want to push to this repository can be placed into a subfolder of this folder to be included in the `.gitignore`. )
