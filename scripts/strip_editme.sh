#!/bin/bash

# Set the default folder to the current directory if no argument is provided
folder_path="${1:-.}"

# Check if the specified folder exists
if [ ! -d "$folder_path" ]; then
    echo "Error: Folder not found: $folder_path"
    exit 1
fi

# Use find to locate all Markdown files in the folder and its subfolders
find "$folder_path" -type f -name "*.md" | while read -r file; do
    # Use sed to remove text between `EDITME` and the next closing parenthesis
    sed -i 's/`EDITME`([^)]*)//g' "$file"
done
